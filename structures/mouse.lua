function pilcrow.mouse.getRelativeMousePosition()
  local mouseX, mouseY = love.mouse.getPosition()

  --player is always in the middle, so we use that as a reference
  local player = pilcrow.GameHelper.getInstance():getPlayer()
  local playerX = player:getX()
  local playerY = player:getY()

  mouseX = mouseX + (playerX - love.graphics.getWidth() / 2)
  mouseY = mouseY + (playerY - love.graphics.getHeight() / 2)

  return mouseX, mouseY
end
