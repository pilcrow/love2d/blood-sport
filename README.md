# Blood Sport
An endless survival arena shooter game made as a teamwork learning exercise.

## Features
 - An arena
 - Lots of weapons
 - Power-ups
 - Cool synthwave music
 - Different types of enemies
 - A couple of maps
 - Bad graphics
 - Awful level design
 
## Technical stuff
 - Made using LÖVE framework
 - Lua
 - Agile development (Scrum)
 - Object-oriented
 - Lots of custom algorithms
 
## How to run

  - [Download and install LÖVE](https://love2d.org/)
  - [Follow instructions in "Running Games" section](https://love2d.org/wiki/Getting_Started)

[Or build an executable](https://love2d.org/wiki/Building_L%C3%96VE)
 
## Credits
 - LÖVE framework (https://love2d.org/)
 - STI (https://github.com/karai17/Simple-Tiled-Implementation)
 - Classic (https://github.com/rxi/classic)


## Screenshots
![game screenshot](./screenshots/game1.png "Game")
![game screenshot](./screenshots/game2.png "Game")
![game screenshot](./screenshots/game3.png "Game")
![game screenshot](./screenshots/game4.png "Game")
![menu](./screenshots/menu.png "Menu")
![shop](./screenshots/shop.png "Shop")
![game over](./screenshots/gameOver.png "Game Over")
