pilcrow.debug.GameScreenHandler.screenChange = true

pilcrow.debug.GameHelper.newMap = true

pilcrow.debug.MapHelper.enemySpawn = true

pilcrow.debug.Creature.enableHitBoxDraw = true

pilcrow.debug.Player.godMode = true

function pilcrow.debug.print(object, actionName, parameterTable)
  local printString = "|" .. tostring(object) .. "|"

  printString = printString .. actionName .. "|"

  for k, v in pairs(parameterTable) do
    printString = printString .. k .. ":" .. v .. "; "
  end

  print(printString)
end
