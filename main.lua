function love.load(args)
  pilcrow = {}
  pilcrow.debug = {}
  pilcrow.debug.GameScreenHandler = {}
  pilcrow.debug.GameHelper = {}
  pilcrow.debug.MapHelper = {}
  pilcrow.debug.Creature = {}
  pilcrow.debug.Player = {}
  if args[#args] == "debug" then
    require("debugSettings")
  end
  love.window.setFullscreen(true)
  require("load")
  pilcrow.changeGameScreen("MainMenu")
end
