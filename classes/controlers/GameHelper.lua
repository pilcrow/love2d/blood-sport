--[[------------------------------------------------
  SINGLETON CLASS
]]--------------------------------------------------

local GameHelper = Object:extend()
local MapHelper = require("classes/controlers/MapHelper")

function GameHelper:new()
  local mapsData = {
    {name = "sand", enemyTypes = {pilcrow.creatures.FatCyborg, pilcrow.creatures.Drone, pilcrow.creatures.Mech, pilcrow.creatures.YakuzaGoon, pilcrow.creatures.YakuzaBoss, pilcrow.creatures.Armor1, pilcrow.creatures.Armor2}},
    {name = "office", enemyTypes = {pilcrow.creatures.FatCyborg, pilcrow.creatures.Drone, pilcrow.creatures.Mech, pilcrow.creatures.YakuzaGoon, pilcrow.creatures.YakuzaBoss, pilcrow.creatures.Armor1, pilcrow.creatures.Armor2}},
    {name = "city", enemyTypes = {pilcrow.creatures.FatCyborg, pilcrow.creatures.Drone, pilcrow.creatures.Mech, pilcrow.creatures.YakuzaGoon, pilcrow.creatures.YakuzaBoss, pilcrow.creatures.Armor1, pilcrow.creatures.Armor2}},
    {name = "rooftop", enemyTypes = {pilcrow.creatures.FatCyborg, pilcrow.creatures.Drone, pilcrow.creatures.Mech, pilcrow.creatures.YakuzaGoon, pilcrow.creatures.YakuzaBoss, pilcrow.creatures.Armor1, pilcrow.creatures.Armor2}}
  }
  local mapData = nil
  local mapHelper = nil

  local maxEnemyCount = 5
  local totalEnemies = 10

  local player = pilcrow.creatures.Player(0, 0)

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:addToBulletLayer(bullet)
    table.insert(mapHelper:getMap().layers["bulletLayer"].objects, bullet)
  end

  function self:convertPixelToTile(x, y)
    return mapHelper:getMap():convertPixelToTile(x, y)
  end

  function self:enemyKilled()
    return mapHelper:enemyKilled()
  end

  function self:mapCompleted()
    player:onMapCompleted()
    pilcrow.changeGameScreen("Shop")
  end

  function self:newMap()
    player:onNewMap()
    local level = player:getLevel()
    maxEnemyCount = 5 + (level * 2)
    totalEnemies = 5 + (level ^ 2)

    mapData = mapsData[love.math.random(1, #mapsData)]
    mapHelper = MapHelper(self)

    if pilcrow.debug and pilcrow.debug.GameHelper.newMap then
      pilcrow.debug.print(self, "New map created", {["Max enemies"] = maxEnemyCount, ["TotalEnemies"] = totalEnemies})
    end
  end

  function self:gameOver()
    player = pilcrow.creatures.Player(0, 0)
    pilcrow.changeGameScreen("GameOver")
  end

  function self:getEnemies()
    return self:getMap().layers["enemyLayer"].objects
  end

  function self:getEnemyTypes()
    return mapData.enemyTypes
  end

  function self:getMap()
    return mapHelper:getMap()
  end

  function self:getMapName()
    return mapData.name
  end

  function self:getMapPath()
    return "assets/maps/" .. mapData.name ..".lua"
  end

  function self:getMapSoundPath()
    return "assets/sounds/maps/" .. mapData.name ..".mp3"
  end

  function self:getMaxEnemyCount()
    return maxEnemyCount
  end

  function self:getPlayer()
    return player
  end

  function self:getTotalEnemies()
    return totalEnemies
  end

  function self:isEnemyPosition(x, y)
    for key, enemy in pairs(self:getEnemies()) do
      local enemyHeight = enemy:getHeight()
      local enemyWidth = enemy:getWidth()
      local distanceFromPosition = enemy:calculateDistanceFromPoint(x, y)
      if distanceFromPosition > 5 and distanceFromPosition < ((enemyHeight + enemyWidth) / 4) then
        return true
      end
    end
    return false
  end

  function self:isPropPosition(x, y)
    local tileX, tileY = self:convertPixelToTile(x, y)
    --tile positions need to be integers
    tileX = math.ceil(tileX)
    tileY = math.ceil(tileY)
    return self:isPropTilePosition(tileX, tileY)
  end

  function self:isPropTilePosition(tileX, tileY)
    return not self:isTilePassable("props", tileX, tileY)
  end

  function self:isTilePassable(layerName, tileX, tileY)
    local _, fractionX = math.modf(tileX)
    local _, fractionY = math.modf(tileY)

    if fractionX ~= 0 or fractionY ~= 0 then
      error("Tile position needs to be an integer: " .. tileX .. " " .. tileY, 2)
    end

    local tileProperties = mapHelper:getMap():getTileProperties(layerName, tileX, tileY)
    if tileProperties.impassable then
      return false
    else
      return true
    end
  end

  function self:isWallPosition(x, y)
    local tileX, tileY = self:convertPixelToTile(x, y)
    --tile positions need to be integers
    tileX = math.ceil(tileX)
    tileY = math.ceil(tileY)
    return self:isWallTilePosition(tileX, tileY)
  end

  function self:isWallTilePosition(tileX, tileY)
    return not self:isTilePassable("walls", tileX, tileY)
  end

  function self:spawnEnemies(dt)
    return mapHelper:spawnEnemies(dt)
  end
end

--singleton creation
local gameHelper = GameHelper()
GameHelper.__instance = gameHelper

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function GameHelper.__tostring()
  return "GameHelper"
end

--[[------------------------------------------------
  STATIC FUNCTIONS
]]--------------------------------------------------

function pilcrow.GameHelper.getInstance()
  return GameHelper.__instance
end
