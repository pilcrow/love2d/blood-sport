--[[------------------------------------------------
  COMPONENT CLASS
]]--------------------------------------------------
--this class is only meant to be instantiated by GameHelper, do not `require()` it anywhere else
local MapHelper = Object:extend()

function MapHelper:new(gameHelper)
  do
    local errorMessage = "Tried to create MapHelper with invalid "
    if tostring(gameHelper) ~= "GameHelper" then
      error(errorMessage .. "GameHelper", 2)
    end
  end

  local gameHelper = gameHelper

  local name = mapName
  local map = STI(gameHelper:getMapPath())

  local enemySpawns = {} -- [index] = {x,y}
  local enemyTypes = gameHelper:getEnemyTypes()
  local enemyCount = 0
  local maxEnemyCount = gameHelper:getMaxEnemyCount()
  local totalEnemies = gameHelper:getTotalEnemies()
  local enemiesKilled = 0

  --[[------------------------------------------------
    FUNCTIONS
  ]]--------------------------------------------------

  function self:enemyKilled()
    enemiesKilled = enemiesKilled + 1
    enemyCount = enemyCount - 1
  end

  function self:getMap()
    return map
  end

  function self:getName()
    return name
  end

  function self:getPlayerSpawnPosition()
    return playerSpawnX, playerSpawnY
  end

  function self:getEnemySpawnPosition(index)
    --random because no index was provided
    index = index or math.random(1, #enemySpawns)
    local spawnX = enemySpawns[index].x
    local spawnY = enemySpawns[index].y
    return spawnX, spawnY
  end

  function self:getEnemySpawns()
    return enemySpawns
  end

  function self:isSpawnOccupied(x, y)
    -- if an enemy is close to spawn coordinates
    for k, enemy in pairs(self:getMap().layers["enemyLayer"].objects) do
      local distance = enemy:calculateDistanceFromPoint(x, y)
      if distance <= 100 then
        return true
      end
    end
    return false
  end

  --select three furthest spawns (from the player) and spawn an enemy in one of them
  function self:spawnEnemies(dt)
    if enemyCount > 10 then return end

    if enemiesKilled >= totalEnemies then
      gameHelper:mapCompleted()
    end

    if enemiesKilled + enemyCount >= totalEnemies then return end

    local enemySpawns = self:getEnemySpawns()
    local player = pilcrow.GameHelper.getInstance():getPlayer()

    local furthestSpawns = {}

    for i, spawn in ipairs(enemySpawns) do
      if #furthestSpawns < 3 then
        table.insert(furthestSpawns, spawn)
      else
        local spawnDistance = player:calculateDistanceFromPoint(spawn.x, spawn.y)
        for i, furthestSpawn in ipairs(furthestSpawns) do
          local furthestSpawnDistance = player:calculateDistanceFromPoint(furthestSpawn.x, furthestSpawn.y)
          if spawnDistance > furthestSpawnDistance then
            furthestSpawns[i] = spawn
            break
          end
        end
      end
    end

    local spawnIndex = 1

    repeat
      local spawnOccupied = self:isSpawnOccupied(furthestSpawns[spawnIndex].x, furthestSpawns[spawnIndex].y)
      if spawnOccupied then
        spawnIndex = spawnIndex + 1
      end
    until(not spawnOccupied or spawnIndex > #furthestSpawns)

    --don't spawn because all spawns are occupied
    if spawnIndex > #furthestSpawns then
      return
    end

    local enemyType = math.random(1, #enemyTypes)
    local enemy = enemyTypes[enemyType](furthestSpawns[spawnIndex].x, furthestSpawns[spawnIndex].y)
    table.insert(map.layers["enemyLayer"].objects, enemy)

    enemyCount = enemyCount + 1

    if pilcrow.debug and pilcrow.debug.MapHelper.enemySpawn then
      pilcrow.debug.print(self, "Spawned", {["Enemy"] = tostring(enemy), ["EnemyCount"] = enemyCount})
    end
  end

  --[[------------------------------------------------
    INITIALIZATION LOGIC
  ]]--------------------------------------------------

  --Load player layer
  do
    --get player's spawn position and save the data to mapHelper
    local playerSpawnObject
    for k, object in pairs(map.objects) do
      if object.name == "player" then
        playerSpawnObject = object
        break
      end
    end
    playerSpawnX = playerSpawnObject.x
    playerSpawnY = playerSpawnObject.y
    map.layers["playerSpawn"].visible = false

    local mapsName = gameHelper:getMapName()
    local playerLayer
    if mapsName == "rooftop" or mapsName == "city" then
      playerLayer = map:addCustomLayer("playerLayer")
    else
      playerLayer = map:addCustomLayer("playerLayer", 5)
    end

    local player = pilcrow.GameHelper.getInstance():getPlayer()
    player:setX(playerSpawnX)
    player:setY(playerSpawnY)


    playerLayer.objects = {}
    playerLayer.bullets = {}
    table.insert(playerLayer.objects, player)

    playerLayer.draw = function(self)
      for k, object in pairs(self.objects) do
        object:draw()
      end
    end

    playerLayer.update = function(self, dt)
      local player = pilcrow.GameHelper.getInstance():getPlayer()
      player:update(dt)
    end

  end

  --Load enemy layer
  do
    for k, enemySpawn in pairs(map.layers["enemySpawn"].objects) do
      local spawn = {}
      spawn.x = enemySpawn.x
      spawn.y = enemySpawn.y
      table.insert(enemySpawns, spawn)
    end
    local enemyLayer
    enemyLayer = map:addCustomLayer("enemyLayer")
    enemyLayer.objects = {}
    enemyLayer.bullets = {}

    enemyLayer.draw = function(self)
      for k, enemy in pairs(self.objects) do
        enemy:draw()
      end
    end

    enemyLayer.update = function(self, dt)
      for k, enemy in pairs(self.objects) do
        enemy:update(dt)
      end
    end

    map.layers["enemySpawn"].visible = false
  end

  --Create bullet layer
  do
    local bulletLayer
    bulletLayer = map:addCustomLayer("bulletLayer")
    bulletLayer.objects = {}

    bulletLayer.draw = function(self)
      for k, bullet in pairs(self.objects) do
        bullet:draw()
      end
    end

    bulletLayer.update = function(self, dt)
      for k, bullet in pairs(self.objects) do
        bullet:update(dt)
        if bullet:isMarkedForRemoval() then
          self.objects[k] = nil
        end
      end
    end
  end
end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function MapHelper.__tostring()
  return "MapHelper"
end

return MapHelper
