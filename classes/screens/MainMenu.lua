local MainMenu = Screen:extend()

function MainMenu:new()
  local visibleTextBoxes = {}
  local window = nil
  local textBoxesMain = {}
  local textBoxesSettings = {}
  local textBoxesCredits = {}

  local titleTextBox

  local titleFont = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.04)
  local menuFont = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.025)
  local sound = love.audio.newSource("assets/sounds/screens/MainMenu.mp3", "static")

  love.audio.play(sound)

  --[[------------------------------------------------
    PRIVATE FUNCTIONS
  ]]--------------------------------------------------

  local function drawTitle(textBox)
    textBox:draw(love.graphics.getWidth() * 0.1, love.graphics.getHeight() * 0.1)
  end

  local function drawExitButton(textBox)
    textBox:draw(love.graphics.getWidth() * 0.1, love.graphics.getHeight() * 0.9)
  end

  local function showMain()
    visibleTextBoxes = textBoxesMain
  end

  local function showSettings()
    visibleTextBoxes = textBoxesSettings
  end

  local function showCredits()
    visibleTextBoxes = textBoxesCredits
  end

  local function closeWindow()
    window = nil
  end

  local function createResolutionPickerWindow()
    local resolutionPickerWindow = Window(love.graphics.getWidth() * 0.5, love.graphics.getHeight() * 0.5)

    resolutionPickerWindow:addTextBox(TextBox(menuFont, "1920x1080", function()
                                                                      love.window.setMode(1920,1080)
                                                                    end))
    resolutionPickerWindow:addTextBox(TextBox(menuFont, "1600x900", function()
                                                                      love.window.setMode(1600,900)
                                                                    end))
    resolutionPickerWindow:addTextBox(TextBox(menuFont, pilcrow.Language:getText("back"), closeWindow))
    resolutionPickerWindow:setTextBoxesColor(1, 1, 1, 1)
    window = resolutionPickerWindow
  end

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:draw()
    love.graphics.setBackgroundColor(1, 1, 1, 1)
    love.graphics.setColor(0, 0, 0)

    for i, textBox in ipairs(visibleTextBoxes) do
      if i == 1 then
        drawTitle(textBox)
      elseif i == #visibleTextBoxes then
        drawExitButton(textBox)
      else
        local y = love.graphics.getHeight() * 0.2
        y = y + (love.graphics.getHeight() * 0.1 * i)
        textBox:draw(love.graphics.getWidth() * 0.1, y)
      end
    end

    if window then
      window:draw()
    end
  end

  function self:finalize()
    visibleTextBoxes = nil
    window = nil
    textBoxesMain = nil
    textBoxesSettings = nil
    textBoxesCredits = nil
    love.audio.stop(sound)
  end

  function self:mouseReleased(x, y, button, isTouch)
    if window then
      for i,textBox in ipairs(window:getTextBoxes()) do
        if textBox:isHovered() then
          textBox:onClick()
          return
        end
      end
    else
      for k, textBox in pairs(visibleTextBoxes) do
        if textBox:isHovered() then
          textBox:onClick()
          return
        end
      end
    end
  end

  --[[------------------------------------------------
    CONSTRUCTOR
  ]]--------------------------------------------------

  titleTextBox = TextBox(titleFont, "Blood Sport")
  titleTextBox:setColor(1,0,0,1)
  titleTextBox:setHoverable(false)
  table.insert(textBoxesMain, titleTextBox)
  table.insert(textBoxesMain, TextBox(menuFont, pilcrow.Language:getText("newGame"), function() pilcrow.changeGameScreen("Game") end))
  table.insert(textBoxesMain, TextBox(menuFont, pilcrow.Language:getText("settings"), showSettings))
  table.insert(textBoxesMain, TextBox(menuFont, pilcrow.Language:getText("credits"), showCredits))
  table.insert(textBoxesMain, TextBox(menuFont, pilcrow.Language:getText("exit"), function() love.event.quit(0) end))

  table.insert(textBoxesSettings, titleTextBox)
  table.insert(textBoxesSettings, TextBox(menuFont, pilcrow.Language:getText("enableFullscreen"),
                                          function(self)
                                            local fullscreen, _ =  love.window.getFullscreen()
                                            if fullscreen then
                                              love.window.setFullscreen(false)
                                              self:setString(pilcrow.Language:getText("enableFullscreen"))
                                            else
                                              love.window.setFullscreen(true)
                                              self:setString(pilcrow.Language:getText("disableFullscreen"))
                                            end
                                          end))
  table.insert(textBoxesSettings, TextBox(menuFont, pilcrow.Language:getText("changeResolution"), createResolutionPickerWindow))
  table.insert(textBoxesSettings, TextBox(menuFont, pilcrow.Language:getText("back"), showMain))
  table.insert(textBoxesCredits, titleTextBox)
  table.insert(textBoxesCredits, TextBox(menuFont, pilcrow.Language:getText("programmers")))
  table.insert(textBoxesCredits, TextBox(menuFont,"Patryk Najdychor"))
  table.insert(textBoxesCredits, TextBox(menuFont,"Bartosz Groffik"))
  table.insert(textBoxesCredits, TextBox(menuFont, pilcrow.Language:getText("audioDesign")))
  table.insert(textBoxesCredits, TextBox(menuFont,"Alex Jadwiszczok"))
  table.insert(textBoxesCredits, TextBox(menuFont, pilcrow.Language:getText("back"), showMain))

  for k,v in ipairs(textBoxesCredits) do
    if not (k == #textBoxesCredits) then
      v:setHoverable(false)
    end
  end

  visibleTextBoxes = textBoxesMain
end

pilcrow.Screens.MainMenu = MainMenu
