ShopSection = pilcrow.Object:extend()

function ShopSection:new(protected, title, index)
     local title = title
     local index = index
     local borderWidth = 17
     local height = (love.graphics.getHeight() * 0.74)
     local width = ((love.graphics.getWidth() * 0.7) - 25) * 0.8 - borderWidth * 2
     local x = 0
     local y = (love.graphics.getHeight() * 0.26)
     local shopItems = {}

 function self:draw()
     love.graphics.setColor(191/255, 191/255, 191/255)
     love.graphics.rectangle("fill", x, y, width, height)
     self:drawTitle()
     self:drawBorder()
     if index ~= 3 then return end
     self:drawShopItems()
end

function self:update(givenX, givenWidth)
     x = givenX
     width = givenWidth
end

function self:drawTitle()
     love.graphics.setColor(0, 0, 0)
     if index == 1 or index == 2 then
          local titleFont = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.04)
          local titleToDraw = love.graphics.newText(titleFont, title)
          local textWidth = titleToDraw:getHeight()
          love.graphics.draw(titleToDraw, (width + textWidth) / 2 + x, y + 20 , 1.57079633)
     elseif index == 3 then
          local titleFont = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.03)
          local titleToDraw = love.graphics.newText(titleFont, title)
          love.graphics.draw(titleToDraw, love.graphics.getWidth() * 0.008 +  x , y)
     end
end

function self:addShopItem(shopItem)
     table.insert(shopItems, shopItem)
end

function self:drawShopItems()
     if #shopItems == 0 then return end
     for a, shopItem in ipairs(shopItems) do
          shopItem:draw()
     end
end

function self:clickItem()
     for a, shopItem in ipairs(shopItems) do
          if shopItem:isHovered() then
               shopItem:onClick()
          end
     end
end

function self:drawBorder()
     if index == 3 then return end
     love.graphics.setColor(0, 0, 0)
     love.graphics.rectangle("fill", x + width, y, borderWidth, height)
end

function self:getBorderWidth()
     return borderWidth
end

function self:getIndex()
     return index
end

function self:getName()
     return title
end
function self:isHovered()
     local mouseX, mouseY = love.mouse.getPosition()
     if mouseX > x and mouseX < (x + width) and mouseY > y and mouseY < (y + height) then
          return true
     else
          return false
     end
end

function self:changeIndex(givenIndex)
     index = givenIndex
end

end
