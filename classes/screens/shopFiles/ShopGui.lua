ShopGui = pilcrow.Object:extend()

function ShopGui:new(protected)
     local guiWidth = (love.graphics.getWidth() * 0.7)
     local guiHeight = (love.graphics.getHeight() * 0.26) - 25
     local playerStats = {
          health = 0,
          maxHealth = 100,
          points = 0,
          level = 0
     }
     function self:draw()
          local lineWidth = 25
          self:updatePlayerStats()
          -- silver color
          love.graphics.setColor(191 / 255, 191 / 255, 191 / 255)
          love.graphics.rectangle("fill", 0, 0, guiWidth, guiHeight)

          -- black color
          love.graphics.setColor(0, 0, 0)
          love.graphics.rectangle("fill", (love.graphics.getWidth() * 0.7) - lineWidth, 0, lineWidth, love.graphics.getHeight())
          love.graphics.rectangle("fill", 0,(love.graphics.getHeight() * 0.26) - lineWidth, love.graphics.getWidth() * 0.7, lineWidth)
          -- Player ico
          love.graphics.rectangle("fill", guiWidth * 0.015, guiWidth * 0.015, guiWidth * 0.2, (guiHeight-(guiWidth * 0.03)))
          -- Health Bar
          love.graphics.rectangle("fill", guiWidth * 0.23, guiWidth * 0.015, guiWidth * 0.45, (guiHeight - (guiWidth * 0.03)) * 0.38)

          -- white color
          love.graphics.setColor(1, 1, 1)
          -- Player ico
          love.graphics.rectangle("fill", guiWidth * 0.015 + 10, guiWidth * 0.015 + 10, guiWidth * 0.2 - 20, guiHeight - guiWidth * 0.03 - 20)
          -- Health Bar
          love.graphics.rectangle("fill", guiWidth * 0.23 + 10, guiWidth * 0.015 + 10, guiWidth * 0.45 - 20, (guiHeight - (guiWidth * 0.03)) * 0.38 - 20)
          self:drawHealthBar(playerStats.health,playerStats.maxHealth)
          self:drawPlayerStats(playerStats.points,playerStats.level)
     end

     function self:updatePlayerStats()
          local player = pilcrow.GameHelper.getInstance():getPlayer()
          playerStats.points = pilcrow.GameHelper.getInstance():getPlayer():getPoints()
          playerStats.level = pilcrow.GameHelper.getInstance():getPlayer():getLevel()
          playerStats.health = pilcrow.GameHelper.getInstance():getPlayer():getHealth()
     end

     function self:drawHealthBar(hp,maxHp)
          local hpPercent = hp / maxHp
          love.graphics.setColor(1, 0, 0)
          love.graphics.rectangle("fill", guiWidth * 0.23 + 10, guiWidth * 0.015 + 10, (guiWidth * 0.45 - 20) * hpPercent, (guiHeight - (guiWidth * 0.03)) * 0.38 - 20)
          love.graphics.setColor(0, 0, 0)
          local hpToDisplay = "HP: " .. hp .. " / " .. maxHp
          local font =  love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.035)
          local hpToDraw = love.graphics.newText(font, hpToDisplay)
          love.graphics.draw(hpToDraw, guiWidth * 0.23, guiWidth * 0.02 + (guiHeight - (guiWidth * 0.03)) * 0.38)
     end

     function self:drawPlayerStats(points,lvl)
          love.graphics.setColor(0, 0, 0)
          local pointsToDisplay = "Points: " .. points
          local lvlToDisplay = "Lvl: " .. lvl
          local font =  love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.032)
          local pointsToDraw = love.graphics.newText(font, pointsToDisplay)
          local lvlToDraw = love.graphics.newText(font, lvlToDisplay)
          love.graphics.draw(pointsToDraw, guiWidth * 0.69, guiWidth * 0.005)
          love.graphics.draw(lvlToDraw, guiWidth * 0.69, pointsToDraw:getHeight())
     end
end
