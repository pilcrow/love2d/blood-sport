local Game = Screen:extend()

function Game:new()
  local map

  --should be nilled when Game is not in pause mode
  local pauseGameComponent = nil

  local mapMusic
  local pauseMusic

  self.bindings = {
    up = function() pilcrow.GameHelper.getInstance():getPlayer():setMoveUp(true) end,
    down = function() pilcrow.GameHelper.getInstance():getPlayer():setMoveDown(true) end,
    left = function() pilcrow.GameHelper.getInstance():getPlayer():setMoveLeft(true) end,
    right = function() pilcrow.GameHelper.getInstance():getPlayer():setMoveRight(true) end,
    escape =
      function()
        if not self:isPaused() then
          self:pause()
        else
          self:unPause()
        end
      end
  }

  self.bindingsReleased = {
    up = function() pilcrow.GameHelper.getInstance():getPlayer():setMoveUp(false) end,
    down = function() pilcrow.GameHelper.getInstance():getPlayer():setMoveDown(false) end,
    left = function() pilcrow.GameHelper.getInstance():getPlayer():setMoveLeft(false) end,
    right = function() pilcrow.GameHelper.getInstance():getPlayer():setMoveRight(false) end,
  }

  self.keys = {
    w = "up",
    s = "down",
    a = "left",
    d = "right",
    escape = "escape"
  }

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:draw()
    -- Scale world
    local scale = math.floor(love.graphics.getWidth() * 0.00195)
    local screenWidth = love.graphics.getWidth() / scale
    local screenHeight = love.graphics.getHeight() / scale

    -- Translate world so that player is always centred
    local player = map.layers["playerLayer"].objects[1]
    local tx = math.floor(player:getX() - screenWidth / 2)
    local ty = math.floor(player:getY() - screenHeight / 2)

    -- Draw world
    map:draw(-tx, -ty, scale, scale)

    --this is drawn at the end to prevent scaling
    pilcrow.GameHelper.getInstance():getPlayer():drawStats()

    --Reset the color
    love.graphics.setColor(1, 1, 1)
  end

  function self:finalize()
    if mapMusic then love.audio.stop(mapMusic) end
    if pauseMusic then love.audio.stop(pauseMusic) end
  end

  function self:getBinding(name)
    return bindings[name]
  end

  function self:getBindingReleased(name)
    return bindingsReleased[name]
  end

  function self:getKey(key)
    return keys[key]
  end

  function self:isPaused()
    if pauseGameComponent then
      return true
    end
    return false
  end

  function self:pause()
    pauseGameComponent = pilcrow.screens.PauseGameComponent(self)

    if mapMusic then
      love.audio.pause(mapMusic)
    end
    love.audio.play(pauseMusic)
  end

  function self:unPause()
    pauseGameComponent:unPause()
    pauseGameComponent = nil

    love.audio.stop(pauseMusic)
    love.audio.play(mapMusic)
  end

  function self:update(dt)
    pilcrow.GameHelper.getInstance():spawnEnemies(dt)
    map:update(dt)
  end

  --[[------------------------------------------------
    CONSTRUCTOR
  ]]--------------------------------------------------

  local cursor = love.mouse.newCursor("assets/sprites/cursor1.png", 16, 16)
  love.mouse.setCursor(cursor)

  local gameHelper = pilcrow.GameHelper.getInstance()
  gameHelper:newMap()
  map = gameHelper:getMap()

  local mapSoundPath = gameHelper:getMapSoundPath()
  --returns nil if file doesn't exist
  if love.filesystem.getInfo(mapSoundPath) then
    mapMusic = love.audio.newSource(gameHelper:getMapSoundPath(), "static")
  end
  --pause music is here (instead of on PauseGameComponent) to prevent loading lag
  pauseMusic = love.audio.newSource("assets/sounds/screens/PauseMusic.mp3", "static")
  if mapMusic then
    love.audio.play(mapMusic)
  end
end

pilcrow.Screens.Game = Game
