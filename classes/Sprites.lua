--[[------------------------------------------------
  COMPONENT CLASS
]]--------------------------------------------------
Sprites = Object:extend()

function Sprites:new(fileNamePrefix, speed, frameWidth, frameHeight)
  do
    local errorMessage = "Tried to create a Sprites with invalid "
    if speed <= 0 then
      error(errorMessage .. "speed " .. speed, 2)
    end
    if frameWidth <= 0 then
      error(errorMessage .. "frameWidth " .. frameWidth, 2)
    end
    if frameHeight <= 0 then
      error(errorMessage .. "frameHeight " .. frameHeight, 2)
    end
  end

  self.animationSpeedConstant = 85

  self.spriteSheetLeft = love.graphics.newImage(fileNamePrefix .. "Left.png")
  self.spriteSheetUp = love.graphics.newImage(fileNamePrefix .. "Up.png")
  self.spriteSheetRight = love.graphics.newImage(fileNamePrefix .. "Right.png")
  self.spriteSheetDown = love.graphics.newImage(fileNamePrefix .. "Down.png")
  self.spriteSheetIdle = love.graphics.newImage(fileNamePrefix .. "Idle.png")


  self.frameHeight = frameHeight
  self.frameWidth = frameWidth

  self.quadLeft = love.graphics.newQuad(0, 0, self.frameWidth, self.frameHeight, self.spriteSheetLeft:getDimensions())
  self.quadUp = love.graphics.newQuad(0, 0, self.frameWidth, self.frameHeight, self.spriteSheetUp:getDimensions())
  self.quadRight = love.graphics.newQuad(0, 0, self.frameWidth, self.frameHeight, self.spriteSheetRight:getDimensions())
  self.quadDown = love.graphics.newQuad(0, 0, self.frameWidth, self.frameHeight, self.spriteSheetDown:getDimensions())
  self.quadIdle = love.graphics.newQuad(0, 0, self.frameWidth, self.frameHeight, self.spriteSheetIdle:getDimensions())

  self.direction = "idle"

  self.currentFrame = 0
  self.lastAnimationTime = 0

  self:setSpeed(speed)
end

function Sprites:getImage()
  if self.direction == "idle" then
    return self.spriteSheetIdle
  elseif self.direction == "left" then
    return self.spriteSheetLeft
  elseif self.direction == "up" then
    return self.spriteSheetUp
  elseif self.direction == "right" then
    return self.spriteSheetRight
  elseif self.direction == "down" then
    return self.spriteSheetDown
  end
end

function Sprites:getHeight()
  return self.frameHeight
end

function Sprites:getQuad()
  if self.direction == "idle" then
    return self.quadIdle
  elseif self.direction == "left" then
    return self.quadLeft
  elseif self.direction == "up" then
    return self.quadUp
  elseif self.direction == "right" then
    return self.quadRight
  elseif self.direction == "down" then
    return self.quadDown
  end
end

function Sprites:getWidth()
  return self.frameWidth
end

function Sprites:setDirection(direction)
  self.direction = direction
end

function Sprites:setSpeed(speed)
  self.animationSpeed = self.animationSpeedConstant /(speed * (self.spriteSheetLeft:getDimensions() / self.frameWidth))
end

function Sprites:update(dt)
  self.lastAnimationTime = self.lastAnimationTime + dt

  if self.lastAnimationTime > self.animationSpeed then
    self.currentFrame = self.currentFrame + 1
    local nextImagePosition = self.frameWidth * self.currentFrame
    if nextImagePosition >= self:getImage():getWidth() then
      nextImagePosition = 0
      self.currentFrame = 0
    end
    self:getQuad():setViewport(nextImagePosition, 0, self.frameWidth, self.frameHeight)
    self.lastAnimationTime = 0
  end
end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function Sprites.__tostring()
  return "Sprites"
end
