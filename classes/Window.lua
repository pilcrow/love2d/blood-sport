Window = Object:extend()

function Window:new(width, height)
  do
    local errorMessage = "Tried to create a Window with invalid "
    if width <= 0 or height <= 0 then
      error(errorMessage .. " width or height " .. width .. " " .. height, 2)
    end
  end

  self.textBoxes = {}
  self.width = width
  self.height = height
  self.visible = true
end

function Window:addTextBox(textBox)
  textBox:setParentWindow(self)
  table.insert(self.textBoxes, textBox)
end

function Window:draw()
  if not self:isVisible() then return love.graphics.clear() end

  love.graphics.setColor(0, 0, 0, 0.8)
  love.graphics.rectangle("fill", love.graphics.getWidth() * 0.5 - self:getW() * 0.5, love.graphics.getHeight() * 0.5 - self:getH() * 0.5, self:getW(), self:getH())

  for i, textBox in ipairs(self.textBoxes) do
    local mouseX, mouseY = love.mouse.getPosition()
    if mouseX > textBox:getX() and mouseX < (textBox:getX() + textBox:getW()) and mouseY > textBox:getY() and mouseY < (textBox:getY() + textBox:getH()) then
      self.hoveredTextBox = textBox
    end
    textBox:draw(love.graphics.getWidth() * 0.5 - (textBox:getW() * 0.5),  love.graphics.getHeight() * 0.5 - self:getH() * 0.5 + (self:getH() * 0.1 * i))
  end
end

function Window:getW()
  return self.width
end

function Window:getH()
  return self.height
end

function Window:getHoveredTextBox()
  return self.hoveredTextBox
end

function Window:getTextBoxes()
  return self.textBoxes
end

function Window:hasTextBoxes()
  if #self.textBoxes > 0 then
    return true
  end
  return false
end

function Window:isVisible()
  return self.visible
end

function Window:setHoveredTextBox(textBox)
  self.hoveredTextBox = textBox
end

function Window:setTextBoxesColor(r, g, b, a)
  for k, textBox in pairs(self:getTextBoxes()) do
    textBox:setColor(r, g, b, a)
  end
end

function Window:setVisible(visible)
  self.visible = visible
end
