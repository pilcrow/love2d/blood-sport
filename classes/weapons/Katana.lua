pilcrow.weapons.Katana = pilcrow.weapons.Weapon:extend()

function pilcrow.weapons.Katana.constructor(owner)
  local weaponBuilder = pilcrow.weapons.WeaponBuilder()
  weaponBuilder:setOwner(owner)

  weaponBuilder:setAutomatic(false)
  weaponBuilder:setBulletSpeed(1000)
  weaponBuilder:setDamage(25)
  weaponBuilder:setSecondsPerShot(0.4)
  weaponBuilder:setSound(love.audio.newSource("/assets/sounds/weapons/katanaHaulOff.mp3", "static"))
  weaponBuilder:setRange(120)

  return {{weaponBuilder}}
end

function pilcrow.weapons.Katana:new(protected, owner)
  protected.bullet = pilcrow.weapons.MeleeBullet
end
