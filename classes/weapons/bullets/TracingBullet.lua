pilcrow.weapons.TracingBullet = pilcrow.weapons.Bullet:extend()

function pilcrow.weapons.TracingBullet.constructor(startX, startY, endX, endY, speed, damage, range, owner)
  return {{startX, startY, endX, endY, speed, damage, range, owner}}
end

function pilcrow.weapons.TracingBullet:new(protected)
  local tracesDrawn = 10
  local positionHistory = {
    x = protected.x,
    y = protected.y
  }

  function self:draw()
    local x = protected.x
    local y = protected.y
    local alpha = 1
    local blue = 0

    for i, positionTable in ipairs(positionHistory) do
      if i % 2 == 1 then
        love.graphics.setColor(1, 0, blue, alpha)

        love.graphics.draw(
          protected.image,
          math.floor(positionTable.x),
          math.floor(positionTable.y),
          0,
          1,
          1,
          0,
          0,
          (i / 2 * 0.07),
          (i / 2 * 0.07)
        )

        alpha = alpha - (i / 2 * 0.035)
        blue = blue + (i / 2 * 0.07)
      end
    end

    --reset colorr protected.isBr protected.isB
    love.graphics.setColor(1, 1, 1, 1)
  end

  function self:update(dt)
    local xTraveled = protected.dx * dt
    local yTraveled = protected.dy * dt

    protected.x = (protected.x + xTraveled)
    protected.y = (protected.y + yTraveled)

    table.insert(positionHistory, 1, {x = protected.x, y = protected.y})

    --tracesDrawn is multiplied by 3 because we draw only at 1/3 of positions
    if #positionHistory > (tracesDrawn * 2) then
      table.remove(positionHistory, #positionHistory)
    end

    protected.distanceTraveled = protected.distanceTraveled + math.abs(xTraveled) + math.abs(yTraveled)
    if protected.isOutOfRange() or protected.isBulletInWall() then
      protected.markForRemoval = true
    end
    protected.tryHit()
  end
end
