pilcrow.weapons.MeleeBullet = pilcrow.weapons.Bullet:extend()

function pilcrow.weapons.MeleeBullet.constructor(startX, startY, endX, endY, speed, damage, range, owner)
  return {{startX, startY, endX, endY, speed, damage, range, owner}}
end

function pilcrow.weapons.MeleeBullet:new(protected)

  --we don't need to draw it for now
  self.draw = function() end

  --[[------------------------------------------------
    PROTECTED FUNCTIONS
  ]]--------------------------------------------------

  function protected.doesHit(entity)
    local spriteW = entity:getWidth()
    local spriteH = entity:getHeight()
    local x = entity:getX() - (spriteW / 2)
    local y = entity:getY() - (spriteH / 2)

    if  protected.x > (x + (protected.range * 0.5))
    and protected.x < ((x + spriteW) - (protected.range * 0.5))
    and protected.y > (y + (protected.range * 0.5))
    and protected.y < ((y + spriteH) - (protected.range * 0.5)) then
      return true
    end

    if  protected.x > x
    and protected.x < (x + spriteW)
    and protected.y > y
    and protected.y < (y + spriteH) then
      return true
    end

    return false
  end

  function protected.tryHit()
    local objects = protected.getHitTargets()

    for k, object in pairs(objects) do
      if protected.doesHit(object) then
        object:hit(self)
      end
    end
  end
end
