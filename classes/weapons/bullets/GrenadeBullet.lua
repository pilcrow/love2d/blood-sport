pilcrow.weapons.GrenadeBullet = pilcrow.weapons.Bullet:extend()

function pilcrow.weapons.GrenadeBullet.constructor(blastRadius, startX, startY, endX, endY, speed, damage, range, owner)
  return {{startX, startY, endX, endY, speed, damage, range, owner}}
end

function pilcrow.weapons.GrenadeBullet:new(protected, blastRadius, damage)
  local blastRadius = blastRadius
  local damagePerLayer = damage
  local explosionSound = love.audio.newSource("/assets/sounds/weapons/grenadeExplosion.mp3", "static")

  function protected.createDamageZonesInGrenadeExplosion(rangeFactor, numberOfLayers, object)
    local rangeDivisor = 1
    local damageDivisor = 1
    local counter = 0
    damagePerLayer = damage/numberOfLayers
    for i = numberOfLayers, 1, -1 do
      --just deal damage each layer
      if protected.isObjectInGrenadeExplosion(object, rangeDivisor) then
        counter = counter + 1
      end
      rangeDivisor = rangeDivisor * rangeFactor -- rangeFactor allows to determine the damage areas of explosion each iteration
    end
    damagePerLayer = damagePerLayer * counter -- damage accumulates up to full damage of a projectile
    object:hit(self)
  end

  local function explode()
    local objects = protected.getHitTargets()
    for k, object in pairs(objects) do
      protected.createDamageZonesInGrenadeExplosion(2, 3, object)
    end
    love.audio.play(explosionSound)
    explosionSound = nil
    protected.markForRemoval = true
  end

  function self:getDamage()
    return damagePerLayer
  end

  function protected.isObjectInGrenadeExplosion(object, rangeFactor)
    local spriteW = object:getWidth()
    local spriteH = object:getHeight()
    local objectXLeft = object:getX() - (spriteW / 2)
    local objectXRight = object:getX() + (spriteW / 2)
    local objectYTop = object:getY() - (spriteH / 2)
    local objectYBottom = object:getY() + (spriteH / 2)
    local bulletX = protected.x
    local bulletY = protected.y
    local rangeFactor = rangeFactor

    --Euclidean distances to simulate the radius of a circle
    if math.sqrt(math.pow(bulletX - objectXLeft, 2) + math.pow(bulletY - objectYTop, 2)) <= blastRadius/rangeFactor
    or math.sqrt(math.pow(bulletX - objectXRight, 2) + math.pow(bulletY - objectYTop, 2)) <= blastRadius/rangeFactor
    or math.sqrt(math.pow(bulletX - objectXLeft, 2) + math.pow(bulletY - objectYBottom, 2)) <= blastRadius/rangeFactor
    or math.sqrt(math.pow(bulletX - objectXRight, 2) + math.pow(bulletY - objectYBottom, 2)) <= blastRadius/rangeFactor then
      return true
    end
    return false

  end

  function protected.tryHit()
    local objects = protected.getHitTargets()

    for k, object in pairs(objects) do
      if protected.doesHit(object) then
        explode()
        return
      end
    end
  end

  function self:update(dt)
    local xTraveled = protected.dx * dt
    local yTraveled = protected.dy * dt
    protected.x = (protected.x + xTraveled)
    protected.y = (protected.y + yTraveled)
    protected.distanceTraveled = protected.distanceTraveled + math.abs(xTraveled) + math.abs(yTraveled)
    if protected.isOutOfRange() or protected.isBulletInWall() then
      explode()
    end
    protected.tryHit()
  end
end
