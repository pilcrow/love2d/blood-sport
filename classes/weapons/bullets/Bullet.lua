pilcrow.weapons.Bullet = pilcrow.Object:extend()

function pilcrow.weapons.Bullet:new(protected, startX, startY, endX, endY, speed, damage, range, owner)
  do
    local errorMessage = "Tried to create a pilcrow.weapons.Bullet with invalid "
    if speed <= 0 then
      error(errorMessage .. " speed " .. speed, 2)
    end
    if damage < 0 then
      error(errorMessage .. " damage " .. damage, 2)
    end
    if range < 0 then
      error(errorMessage .. " range " .. damage, 2)
    end
  end

  local owner = owner
  local speed = speed
  local damage = damage
  local sprites = {}
  local dir = math.atan2(( endY - startY ), ( endX - startX ))

  local gameHelper = pilcrow.GameHelper.getInstance()

  protected.dx = speed * math.cos(dir)
  protected.dy = speed * math.sin(dir)
  protected.x = startX
  protected.y = startY
  protected.range = range
  protected.distanceTraveled = 0
  protected.image = love.graphics.newImage("assets/sprites/bullet1.png")
  protected.markForRemoval = false
  protected.blastRadius = 30

  pilcrow.GameHelper.getInstance():addToBulletLayer(self)

  --[[------------------------------------------------
    PROTECTED FUNCTIONS
  ]]--------------------------------------------------

  function protected.doesHit(entity)
    local spriteW = entity:getWidth()
    local spriteH = entity:getHeight()
    local x = entity:getX() - (spriteW / 2)
    local y = entity:getY() - (spriteH / 2)

    if  protected.x > x
    and protected.x < (x + spriteW)
    and protected.y > y
    and protected.y < (y + spriteH) then
      return true
    end

    return false
  end

  function protected.getHitTargets()
    local objects = {}

    if owner == pilcrow.weapons.Weapon.OWNER.PLAYER then
      objects = pilcrow.GameHelper.getInstance():getEnemies()
    else
      table.insert(objects, pilcrow.GameHelper.getInstance():getPlayer())
    end

    return objects
  end

  function protected.isBulletInWall()
    if pilcrow.GameHelper:getInstance():isWallPosition(protected.x, protected.y) then
      return true
    end
    return false
  end

  function protected.isOutOfRange()
    if protected.distanceTraveled > protected.range then
      return true
    end
    return false
  end

  function protected.tryHit()
    local objects = protected.getHitTargets()

    for k, object in pairs(objects) do
      if protected.doesHit(object) then
        --FIXME pass raw damage to hit object
        object:hit(self)
        protected.markForRemoval = true
        return
      end
    end
  end

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:draw()
    love.graphics.draw(
      protected.image,
      math.floor(protected.x),
      math.floor(protected.y),
      0,
      1,
      1
    )
  end

  function self:getDamage()
    return damage
  end

  function self:isMarkedForRemoval()
    return protected.markForRemoval
  end

  function self:update(dt)
    local xTraveled = protected.dx * dt
    local yTraveled = protected.dy * dt
    protected.x = (protected.x + xTraveled)
    protected.y = (protected.y + yTraveled)
    protected.distanceTraveled = protected.distanceTraveled + math.abs(xTraveled) + math.abs(yTraveled)
    if protected.isOutOfRange() or protected.isBulletInWall() then
      protected.markForRemoval = true
    end
    protected.tryHit()
  end
end
