--[[------------------------------------------------
  ABSTRACT CLASS
]]--------------------------------------------------
pilcrow.weapons.Weapon = pilcrow.Object:extend()

function pilcrow.weapons.Weapon:new(protected, weaponBuilder)
  weaponBuilder:validateParameters()

  if not weaponBuilder.__tostring or weaponBuilder.__tostring() ~= "pilcrow.weapons.WeaponBuilder" then
    error("Attempted to create a Weapon directly, please use WeaponBuilder.build() for that", 3)
  end

  local owner = weaponBuilder:getOwner()
  local sound = weaponBuilder:getSound()
  local automatic = weaponBuilder:getAutomatic()

  protected.bulletSpeed = weaponBuilder:getBulletSpeed()
  protected.damage = weaponBuilder:getDamage()
  protected.range = weaponBuilder:getRange()
  protected.secondsPerShot = weaponBuilder:getSecondsPerShot()
  protected.blastRadius = weaponBuilder:getBlastRadius()

  --leave it high so that the first shot can be fired instantly
  protected.lastShotTime = 100

  --relevant only for Player owned weapons
  --used for automatic weapons
  protected.wasMouseReleased = true

  protected.bullet = pilcrow.weapons.Bullet

  --[[------------------------------------------------
    PROTECTED FUNCTIONS
  ]]--------------------------------------------------

  function protected.newBullet(startX, startY, directionX, directionY)
    return protected.bullet(startX, startY, directionX, directionY, protected.bulletSpeed, protected.damage, protected.range, owner)
  end

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:canShoot()
    if protected.lastShotTime >= self:getSecondsPerShot() then
      return true
    end
    return false
  end

  function self:getBulletSpeed()
    return protected.bulletSpeed
  end

  function self:getDamage()
    return protected.damage
  end

  function self:getOwner()
    return owner
  end

  function self:getRange()
    return protected.range
  end

  function self:getSecondsPerShot()
    return protected.secondsPerShot
  end

  function self:getSound()
    return sound
  end

  function self:shoot(startX, startY, directionX, directionY)
    if not self:canShoot() then return end

    local bullet = protected.newBullet(startX, startY, directionX, directionY)

    love.audio.stop(self:getSound())
    love.audio.play(self:getSound())

    protected.lastShotTime = 0
    protected.wasMouseReleased = false
  end

  --this is called only by the creature that holds this weapon
  function self:update(dt)
    self:updateLastShotTime(dt)
  end

  function self:updateLastShotTime(dt)
    protected.lastShotTime = protected.lastShotTime + dt
  end

  --This overrides functions that differ between the owners
  if owner == pilcrow.weapons.Weapon.OWNER.PLAYER then
    function self:canShoot()
      --for non-automatic weapons
      if not protected.wasMouseReleased and not automatic then
        return false
      end

      if protected.lastShotTime >= self:getSecondsPerShot() then
        return true
      end
      return false
    end

    --this is called only by the creature that holds this weapon
    function self:update(dt)
      self:updateLastShotTime(dt)

      if not love.mouse.isDown(1) then
        protected.wasMouseReleased = true
      end
    end
  end

  function self:upgradePenetratingBullets()
    protected.bullet = pilcrow.weapons.PenetratingBullet
  end
end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.weapons.Weapon:__tostring()
  return "pilcrow.weapons.Weapon"
end

--[[------------------------------------------------
  ENUMERATIONS
]]--------------------------------------------------

pilcrow.weapons.Weapon.OWNER = {}
pilcrow.weapons.Weapon.OWNER.PLAYER = 1
pilcrow.weapons.Weapon.OWNER.ENEMY = 2
