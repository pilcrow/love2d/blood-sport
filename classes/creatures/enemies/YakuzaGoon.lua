pilcrow.creatures.YakuzaGoon = pilcrow.creatures.Enemy:extend()

function pilcrow.creatures.YakuzaGoon.constructor(x, y)
  local speed = 180
  local sprites = Sprites("assets/sprites/Yakuza1SheetWalk", speed, 40, 40)

  local health = 40
  local points = 50
  local targetDistanceFromPlayer = {low = 60, high = 100}
  local ox = 20
  local oy = 20
  local hitSound = love.audio.newSource("/assets/sounds/hit/FatCyborg.wav", "static")
  local deathSound = love.audio.newSource("/assets/sounds/death/FatCyborg.wav", "static")

  return {{x, y, sprites, health, speed, points, ox, oy}, {targetDistanceFromPlayer, hitSound, deathSound}}
end

function pilcrow.creatures.YakuzaGoon:new(protected, x, y)

end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.creatures.YakuzaGoon:__tostring()
  return "pilcrow.creatures.YakuzaGoon"
end
