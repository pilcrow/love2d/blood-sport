--[[------------------------------------------------
  ABSTRACT CLASS
]]--------------------------------------------------
pilcrow.creatures.Enemy = pilcrow.creatures.Creature:extend()

function pilcrow.creatures.Enemy:new(protected, targetDistanceFromPlayer, hitSound, deathSound)
  do
    local errorMessage = "Tried to create an Enemy with invalid "
    if targetDistanceFromPlayer and type(targetDistanceFromPlayer) ~= "table" then
      error(errorMessage .. " targetDistanceFromPlayer " .. targetDistanceFromPlayer, 2)
    end
  end

  local weapon = pilcrow.weapons.Pistol(pilcrow.weapons.Weapon.OWNER.ENEMY)

  local targetDistanceFromPlayer = targetDistanceFromPlayer or {low = 100, high = 110}

  local hitSound = hitSound
  local deathSound = deathSound

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  --called by Bullet:tryHit()
  function self:hit(bullet)
    self:subtractHealth(bullet:getDamage())

    if self:getHealth() <= 0 then
      love.audio.play(deathSound)
      self:remove(self)
    else
      love.audio.play(hitSound)
    end
  end

  function self:isInTargetDistanceFromPlayer(distance)
    if distance > targetDistanceFromPlayer.low and distance < targetDistanceFromPlayer.high then
      return true
    end
    return false
  end

  function self:remove(compositeEnemy)
    local map = pilcrow.GameHelper.getInstance():getMap()
    for k, enemy in pairs(map.layers["enemyLayer"].objects) do
      if enemy == compositeEnemy then
        map.layers["enemyLayer"].objects[k] = nil
      end
    end
    pilcrow.GameHelper.getInstance():getPlayer():addPoints(compositeEnemy:getPoints())
    pilcrow.GameHelper.getInstance():enemyKilled()
  end

  function self:updateShoot()
    if not self:isInTargetDistanceFromPlayer(self:calculateDistanceFromPlayer()) then return end

    local startX = self:getX()
    local startY = self:getY()
    local player = pilcrow.GameHelper.getInstance():getPlayer()

    weapon:shoot(self:getX() , self:getY(), player:getX(), player:getY())
  end

  function self:update(dt)
    self:updateMove(dt)
    if self:getSprites() then
      self:getSprites():setDirection(self:getDirection())
      self:getSprites():update(dt)
    end
    self:updateShoot()
    weapon:update(dt)
  end

  function self:updateMove(dt)
    local distance = self:calculateDistanceFromPlayer()
    if self:isInTargetDistanceFromPlayer(distance) then return end

    local player = pilcrow.GameHelper.getInstance():getPlayer()

    local direction = math.atan2(( player:getY() - self:getY() ), ( player:getX() - self:getX() ))
    local dx = self:getSpeed() * math.cos(direction)
    local dy = self:getSpeed() * math.sin(direction)

    local absoluteDx = math.abs(dx)
    local absoluteDy = math.abs(dy)

    if absoluteDx > absoluteDy then
      if dx < 0 then
        self:setDirection("left")
      else
        self:setDirection("right")
      end
    else
      if dy < 0 then
        self:setDirection("up")
      else
        self:setDirection("down")
      end
    end

    local plusX = 0
    local plusY = 0

    if distance > targetDistanceFromPlayer.high then
      plusX = dx * dt
      plusY = dy * dt
    elseif distance < targetDistanceFromPlayer.low then
      plusX = -(dx * dt)
      plusY = -(dy * dt)
    else
      self:setDirection("idle")
      return
    end

    --we need to check x and y separately to allow movement in the other axis
    if not self:canMoveTo(self:getX() + plusX, self:getY()) then
      --this corrects movement in the other direction
      --(move faster on y if blocked on x)
      local yCorrection = plusX
      if plusX > 0 and plusY < 0 then
        yCorrection = -plusX
      elseif plusX < 0 and plusY > 0 then
        yCorrection =  math.abs(plusX)
      end

      if self:canMoveTo(self:getX(), self:getY() + yCorrection) then
        plusY = plusY + (yCorrection)/2
      end

      plusX = 0
    end

    if not self:canMoveTo(self:getX(), self:getY() + plusY) then
      local xCorrection = plusY
      if plusY > 0 and plusX < 0 then
        xCorrection = -plusY
      elseif plusY < 0 and plusX > 0 then
        xCorrection = math.abs(plusY)
      end

      if self:canMoveTo(self:getX() + xCorrection, self:getY()) then
        plusX = plusX + (xCorrection)/2
      end

      plusY = 0
    end

    if plusX == 0 and plusY == 0 then
      self:setDirection("idle")
    end

    self:setX(self:getX() + plusX)
    self:setY(self:getY() + plusY)

  end
end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.creatures.Enemy:__tostring()
  return "pilcrow.creatures.Enemy"
end
