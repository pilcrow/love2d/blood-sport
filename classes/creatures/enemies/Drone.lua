pilcrow.creatures.Drone = pilcrow.creatures.Enemy:extend()

function pilcrow.creatures.Drone.constructor(x, y)
  local speed = 220
  local sprites = Sprites("assets/sprites/droneSheet", speed, 40, 40)

  local health = 50
  local points = 150
  local targetDistanceFromPlayer = {low = 135, high = 150}
  local ox = 20
  local oy = 20
  local hitSound = love.audio.newSource("/assets/sounds/hit/Drone.wav", "static")
  local deathSound = love.audio.newSource("/assets/sounds/death/Drone.wav", "static")

  return {{x, y, sprites, health, speed, points, ox, oy}, {targetDistanceFromPlayer, hitSound, deathSound}}
end

function pilcrow.creatures.Drone:new(protected, x, y)

end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.creatures.Drone:__tostring()
  return "pilcrow.creatures.Drone"
end
