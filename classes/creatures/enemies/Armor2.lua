pilcrow.creatures.Armor2 = pilcrow.creatures.Enemy:extend()

function pilcrow.creatures.Armor2.constructor(x, y)
  local speed = 150
  local sprites = Sprites("assets/sprites/Armor2SheetWalk", speed, 40, 40)

  local health = 120
  local points = 175
  local targetDistanceFromPlayer = {low = 100, high = 110}
  local ox = 20
  local oy = 20
  local hitSound = love.audio.newSource("/assets/sounds/hit/FatCyborg.wav", "static")
  local deathSound = love.audio.newSource("/assets/sounds/death/FatCyborg.wav", "static")

  return {{x, y, sprites, health, speed, points, ox, oy}, {targetDistanceFromPlayer, hitSound, deathSound}}
end

function pilcrow.creatures.Armor2:new(protected, x, y)

end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.creatures.Armor2:__tostring()
  return "pilcrow.creatures.Armor1"
end
