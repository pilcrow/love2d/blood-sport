pilcrow.creatures.FatCyborg = pilcrow.creatures.Enemy:extend()

function pilcrow.creatures.FatCyborg.constructor(x, y)
  local speed = 120

  local sprites = Sprites("assets/sprites/playerSheet", speed, 40, 40)

  local health = 100
  local points = 150
  local targetDistanceFromPlayer = {low = 100, high = 110}
  local ox = 20
  local oy = 20
  local hitSound = love.audio.newSource("/assets/sounds/hit/FatCyborg.wav", "static")
  local deathSound = love.audio.newSource("/assets/sounds/death/FatCyborg.wav", "static")

  return {{x, y, sprites, health, speed, points, ox, oy}, {targetDistanceFromPlayer, hitSound, deathSound}}
end

function pilcrow.creatures.FatCyborg:new(protected, x, y)

end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.creatures.FatCyborg:__tostring()
  return "pilcrow.creatures.FatCyborg"
end
