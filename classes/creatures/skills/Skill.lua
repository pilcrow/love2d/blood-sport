pilcrow.creatures.skills.Skill = pilcrow.Object:extend()

function pilcrow.creatures.skills.Skill:new(protected, owner, skillTree, maxLevel)
  local maxLevel = maxLevel
  local owner = owner
  local skillTree = skillTree

  local level = 0

  function protected.getMaxLevel()
    return maxLevel
  end

  function protected.getOwner()
    return owner
  end

  function protected.getSkillTree()
    return skillTree
  end

  function protected.levelUp()
    level = level + 1
  end

  function self:canUse()
  end

  function self:getLevel()
    return level
  end

  function self:reachedLevelLimit()
    if (level >= maxLevel) then
      return true
    end
  end

  function self:use()
  end

  function self:update(dt)
  end
end

function pilcrow.creatures.skills.Skill.__tostring()
  return "pilcrow.creatures.skills.Skill"
end

pilcrow.creatures.skills.Skill.ACTIVE = 1
pilcrow.creatures.skills.Skill.PASSIVE = 2
