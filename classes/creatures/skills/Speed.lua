pilcrow.creatures.skills.Speed = pilcrow.creatures.skills.Skill:extend()

function pilcrow.creatures.skills.Speed.constructor(owner, skillTree)
  local maxLevel = 5
  return {{owner, skillTree, maxLevel}}
end

function pilcrow.creatures.skills.Speed:new(protected)
  function self:getDisplayName()
    return pilcrow.Language:getText("Speed")
  end

  function self:getName()
    return "Speed"
  end

  function self:getType()
    return pilcrow.creatures.skills.Skill.PASSIVE
  end

  function self:upgrade()
    if self:reachedLevelLimit() then return false end
    protected.levelUp()

    local owner = protected.getOwner()
    local newSpeed = owner:getSpeed() * 1.1
    owner:setSpeed(newSpeed)
  end
end

function pilcrow.creatures.skills.Speed.__tostring()
  return "pilcrow.creatures.skills.Speed"
end
