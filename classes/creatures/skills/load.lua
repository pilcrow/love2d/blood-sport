pilcrow.creatures.skills = {}

require("classes/creatures/skills/Skill")
require("classes/creatures/skills/Speed")
require("classes/creatures/skills/Dodge")
require("classes/creatures/skills/Shield")
require("classes/creatures/skills/Slowdown")
