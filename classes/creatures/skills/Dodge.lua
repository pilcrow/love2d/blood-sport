pilcrow.creatures.skills.Dodge = pilcrow.creatures.skills.Skill:extend()

function pilcrow.creatures.skills.Dodge.constructor(owner, skillTree)
  local maxLevel = 6
  return {{owner, skillTree, maxLevel}}
end

function pilcrow.creatures.skills.Dodge:new(protected)
  local originalOwnerSpeed
  --initially high to allow instant first use
  local secondsSinceUse = 100
  local timeBetweenUses = 1.5

  function self:canUse()
    if secondsSinceUse > timeBetweenUses then
      return true
    end
  end

  function self:getDisplayName()
    return pilcrow.Language:getText("Dodge")
  end

  function self:getName()
    return "Dodge"
  end

  function self:getType()
    return pilcrow.creatures.skills.Skill.ACTIVE
  end

  function self:upgrade()
    if self:reachedLevelLimit() then return false end
    protected.levelUp()

    timeBetweenUses = timeBetweenUses - (self:getLevel() * 0.12)
  end

  function self:use()
    if not self:canUse() then return false end
    secondsSinceUse = 0

    originalOwnerSpeed = protected.getOwner():getSpeed()
    protected.getOwner():setSpeed(800)
  end

  function self:update(dt)
    secondsSinceUse = secondsSinceUse + dt

    if originalOwnerSpeed and secondsSinceUse > 0.12 then
      protected.getOwner():setSpeed(originalOwnerSpeed)
    end
  end
end

function pilcrow.creatures.skills.Dodge.__tostring()
  return "pilcrow.creatures.skills.Dodge"
end
