--[[
  Abstract, subclasses create skills
  THERE SHOULD BE NO DIRECT ACCESS TO SKILLS FROM THE OUTSIDE
--]]

pilcrow.creatures.skillTrees.SkillTree = pilcrow.Object:extend()

function pilcrow.creatures.skillTrees.SkillTree:new(protected, owner, skills)
  protected.owner = owner
  protected.skills = {}

  local function throwNotFoundError(skill)
    error("Skill not found: " .. skill, 3)
  end

  function self:canUpgrade(skillName)
    if not protected.skills[skillName] then
      throwNotFoundError(skillName)
    end

    protected.skills[skillName]:canUpgrade()
  end

  function self:canUse(skillName)
    if not protected.skills[skillName] then
      throwNotFoundError(skillName)
    end

    protected.skills[skillName]:canUse()
  end

  function self:getLevel(skillName)
    if not protected.skills[skillName] then
      throwNotFoundError(skillName)
    end

      protected.skills[skillName]:getLevel()
  end

  function self:update(dt)
    for k, skill in pairs(protected.skills) do
      skill:update(dt)
    end
  end

  function self:upgrade(skillName)
    if not protected.skills[skillName] then
      throwNotFoundError(skillName)
    end

    protected.skills[skillName]:upgrade()
  end

  function self:use(skillName)
    if not protected.skills[skillName] then
      throwNotFoundError(skillName)
    end

    local skill = protected.skills[skillName]
    --zero level skills are not unlocked
    if skill:getLevel() ~= 0 then
      return skill:use()
    end
  end

  --initialize skills
  for i,v in ipairs(skills) do
    local skill = v(owner, self)
    protected.skills[skill:getName()] = skill
  end
end
