pilcrow.creatures.skillTrees.Solo = pilcrow.creatures.skillTrees.SkillTree:extend()

function pilcrow.creatures.skillTrees.Solo.constructor(owner)
  local skills = {}
  table.insert(skills, pilcrow.creatures.skills.Speed)
  table.insert(skills, pilcrow.creatures.skills.Dodge)
  table.insert(skills, pilcrow.creatures.skills.Shield)
  table.insert(skills, pilcrow.creatures.skills.Slowdown)

  return {{owner, skills}}
end

function pilcrow.creatures.skillTrees.Solo:new(protected, owner)

end
