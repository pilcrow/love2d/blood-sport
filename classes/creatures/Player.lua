pilcrow.creatures.Player = pilcrow.creatures.Creature:extend()

function pilcrow.creatures.Player.constructor(x, y)
  local speed = 170
  local sprites = Sprites("assets/sprites/AgentSheetWalk", speed, 40, 40)
  local health = 100
  if pilcrow.debug.Player.godMode then health = 999999 end
  local ox = 20
  local oy = 20
  return {{x, y, sprites, health, speed, __, ox, oy}}
end

function pilcrow.creatures.Player:new(protected, x, y)
  local weapon = pilcrow.weapons.Pistol(pilcrow.weapons.Weapon.OWNER.PLAYER)
  local level = 1
  local points = 0

  local moveUp = false
  local moveDown = false
  local moveLeft = false
  local moveRight = false

  local statsFont = love.graphics.newFont("assets/fonts/nasalization-rg.ttf", love.graphics.getWidth() * 0.025)

  --[[------------------------------------------------
    COMPONENT FUNCTIONS
  ]]--------------------------------------------------

  function self:upgradeBulletSpeed()
    return weapon:upgradeBulletSpeed()
  end

  function self:upgradeDamage()
    return weapon:upgradeDamage()
  end

  function self:upgradeSecondsPerShot()
    return weapon:upgradeSecondsPerShot()
  end

  --one time upgrades
  function self:upgradePenetratingBullets()
    return weapon:upgradePenetratingBullets()
  end

  --[[------------------------------------------------
    PRIVATE FUNCTIONS
  ]]--------------------------------------------------

  local function levelUp()
    level = level + 1
  end

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:addPoints(v)
    points = points + v
  end

  function self:canAfford(v)
    if points - v >= 0 then
      return true
    end
    return false
  end

  function self:drawStats()
    --Health
    local healthText = love.graphics.newText(statsFont, self:getHealth())
    love.graphics.setColor(1, 0, 0)
    love.graphics.draw(
      healthText,
      0,
      0)

    --Points
    local pointsText = love.graphics.newText(statsFont, points)
    love.graphics.setColor(0, 1, 0)
    love.graphics.draw(
      pointsText,
      love.graphics.getWidth( ) - pointsText:getWidth(),
      0)

    --Level
    local levelText = love.graphics.newText(statsFont, "Level: " .. level)
    love.graphics.setColor(0, 1, 0)
    love.graphics.draw(
      levelText,
      love.graphics.getWidth( ) - levelText:getWidth(),
      love.graphics.getHeight() - levelText:getHeight())
  end

  function self:getDirection()
    if moveUp then
      return "up"
    elseif moveDown then
      return "down"
    elseif moveLeft then
      return "left"
    elseif moveRight then
      return "right"
    end

    return "idle"
  end

  function self:getLevel()
    return level
  end

  function self:getPoints()
    return points
  end

  function self:hit(bullet)
    self:subtractHealth(bullet:getDamage())
  end

  --called by GameHelper right before changing screen to shop
  function self:onMapCompleted()
    moveUp = false
    moveDown = false
    moveLeft = false
    moveRight = false
    levelUp()
  end

  --called by GameHelper right before new map is created
  function self:onNewMap()
    self:setPosition(0, 0)
  end

  function self:setMoveUp(v)
    moveUp = v
  end

  function self:setMoveRight(v)
    moveRight = v
  end

  function self:setMoveLeft(v)
    moveLeft = v
  end

  function self:setMoveDown(v)
    moveDown = v
  end

  function self:setWeapon(weap)
    weapon = weap
  end

  function self:shoot()
    --this is also checked on the weapon
    --leave it here for optimization
    if not weapon:canShoot() then return end

    local startX = self:getX()
    local startY = self:getY()

    local mouseX, mouseY = pilcrow.mouse.getRelativeMousePosition()

    weapon:shoot(startX, startY, mouseX, mouseY)
  end

  function self:subtractHealth(amount)
    protected.health = protected.health - amount
    if protected.health <= 0 then
      pilcrow.GameHelper.getInstance():gameOver()
    end
  end

  function self:update(dt)
    self:updateMove(dt)
    if self:getSprites() then
      self:getSprites():setDirection(self:getDirection())
      self:getSprites():update(dt)
    end
    if self:getSkillTree() then
      self:getSkillTree():update(dt)
    end

    weapon:update(dt)
    if love.mouse.isDown(1) then
      self:shoot()
    end
  end

  function self:updateMove(dt)
    local pixelsToMove = self:getSpeed() * dt

    local x = 0
    local y = 0

    if moveUp then
      y = y - pixelsToMove
    end
    if moveDown then
      y = y + pixelsToMove
    end
    if moveLeft then
      if not (y == 0) then
        y = y * 0.75
        x = (x - pixelsToMove) * 0.75
      else
        x = x - pixelsToMove
      end
    end
    if moveRight then
      if not (y == 0) then
        y = y * 0.75
        x = (x + pixelsToMove) * 0.75
      else
        x = x + pixelsToMove
      end
    end

    --we need to check x and y separately to allow movement in the other axis
    if not self:canMoveTo(self:getX() + x, self:getY()) then
      x = 0
    end
    if not self:canMoveTo(self:getX(), self:getY() + y) then
      y = 0
    end

    self:setX(self:getX() + x)
    self:setY(self:getY() + y)
  end
end

--[[------------------------------------------------
  METAFUNCTIONS
]]--------------------------------------------------

function pilcrow.creatures.Player:__tostring()
  return "pilcrow.creatures.Player"
end
