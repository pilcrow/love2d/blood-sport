local Protected = pilcrow.Protected

local Object = {}
Object.__index = Object

function Object.constructor()
end

function Object:new()
end

function Object:extend()
  local cls = {}
  for k, v in pairs(self) do
    if k:find("__") == 1 then
      cls[k] = v
    end
  end
  cls.__index = cls
  cls.superClass = self
  setmetatable(cls, self)
  return cls
end

function Object:is(T)
  local mt = getmetatable(self)
  while mt do
    if mt == T then
      return true
    end
    mt = getmetatable(mt)
  end
  return false
end

function Object:__tostring()
  return "Object"
end

function Object:__call(...)
  local obj = setmetatable({}, self)
  local protected = setmetatable({}, self)

  local superClass = obj.superClass
  local superClasses = {}

  --gathers all superclasses in the inheritance chain
  while superClass do
    table.insert(superClasses, 1, superClass)
    superClass = superClasses[1].superClass
  end

  if #superClasses > 0 then
    local superClassParameters = obj.constructor(...)

    for i, superClass in ipairs(superClasses) do

      if superClassParameters and superClassParameters[i - 1] then
        superClass.new(obj, protected, unpack(superClassParameters[i - 1]))
      else
        superClass.new(obj, protected)
      end

    end
  end

  obj:new(protected, ...)
  return obj
end

return Object
