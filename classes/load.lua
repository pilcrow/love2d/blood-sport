pilcrow = pilcrow or {}

--load all packages
require("classes/objectFramework/load")
require("classes/language/load")
require("classes/screens/load")
require("classes/creatures/load")
require("classes/weapons/load")
require("classes/creatures/skills/load")
require("classes/creatures/skillTrees/load")

require("classes/Sprites")
require("classes/TextBox")
require("classes/Window")


pilcrow.GameHelper = pilcrow.GameHelper or {}
require("classes/controlers/GameHelper")
