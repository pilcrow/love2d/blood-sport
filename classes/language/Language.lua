local Language = Object:extend()

function Language:new()
  local texts = {}

  local languages = {}
  languages.English = "en_US"
  languages.Polski = "pl_PL"
  languages.Espanol = "es_ES"
  languages.Deutsch = "de_DE"
  languages.Francais = "fr_FR"
  languages.Japanese = "ja_JP"

  --[[------------------------------------------------
    PRIVATE FUNCTIONS
  ]]--------------------------------------------------

  local function insertTexts(newTexts)
    for k, v in pairs(newTexts) do
      texts[k] = v
    end
  end

  --[[------------------------------------------------
    PUBLIC FUNCTIONS
  ]]--------------------------------------------------

  function self:getLanguages()
    return languages
  end

  function self:getText(textName)
    return texts[textName] or error("Text not found", 2)
  end

  function self:setLanguage(languageTag)
    local newTexts = require("classes/language/" .. languageTag)
    insertTexts(newTexts)
  end
end

pilcrow.Language = Language()

--always load English first, so if there are missing translations, there will always be some text shown
pilcrow.Language:setLanguage("en_US")
