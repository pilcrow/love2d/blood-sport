local texts = {}

--MainMenu screen
texts.newGame = "Nuevo Juego"
texts.settings = "Configuraciones"
texts.credits = "Créditos"
texts.exit = "Salida"
texts.enableFullscreen = "Habilitar pantalla completa"
texts.disableFullscreen = "Deshabilitar pantalla completa"
texts.changeResolution = "Cambiar resolución"
texts.back = "Atrás"
texts.programmers = "Programadores"
texts.audioDesign = "Diseño de audio"
--PauseMenu screen
texts.resume = "Continuar"
--GameOver Screen
texts.dead = "¡Estas muerto!"
texts.pressToContinue = "Pulse cualquier tecla para continuar"
--Shop screen
texts.shop = "La Tienda"
texts.continue = "Continuar"
texts.buySmg = "Comprar SMG"
texts.upgradeBulletSpeed = "Mejora la velocidad de bala"
texts.upgradeDamage = "Mejora el daño de bala"
texts.upgradeFireRate = "Mejora la velocidad de disparo"
texts.upgradeWeaponsRange = "Mejora el alcance del arma"


return texts
