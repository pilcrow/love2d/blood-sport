local texts = {}

--MainMenu screen
texts.newGame = "Nouveau Jeu"
texts.settings = "Paramètres"
texts.credits = "Crédits"
texts.exit = "Sortie"
texts.enableFullscreen = "Activer le Plein écran"
texts.disableFullscreen = "Désactiver le Plein écran"
texts.changeResolution = "Changer la Résolution"
texts.back = "Retour"
texts.programmers = "Programmeurs"
texts.audioDesign = "Conception Audio"
--PauseMenu screen
texts.resume = "Reprendre"
--GameOver Screen
texts.dead = "Tu es mort!"
texts.pressToContinue = "Appuyez sur n'importe quelle touche pour continuer"
--Shop screen
texts.shop = "Magasin"
texts.continue = "Continuez"
texts.buySmg = "Acheter SMG"
texts.upgradeBulletSpeed = "Améliorer la vitesse de balle"
texts.upgradeDamage = "Améliorer les dégâts"
texts.upgradeFireRate = "Améliorer le taux de feu"
texts.upgradeWeaponsRange = "Améliorer la portée de l'arme"


return texts
