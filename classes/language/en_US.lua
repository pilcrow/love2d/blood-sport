local texts = {}

--MainMenu screen
texts.newGame = "New Game"
texts.settings = "Settings"
texts.credits = "Credits"
texts.exit = "Exit"
texts.enableFullscreen = "Enable Fullscreen"
texts.disableFullscreen = "Disable Fullscreen"
texts.changeResolution = "Change resolution"
texts.back = "Back"
texts.programmers = "Programmers"
texts.audioDesign = "Audio Design"
--PauseMenu screen
texts.resume = "Resume"
--GameOver Screen
texts.dead = "You're DEAD!"
texts.pressToContinue = "Press any key to continue"
--Shop screen
texts.shop = "Shop"
texts.continue = "Continue"
texts.buySmg = "Buy SMG"
texts.upgradeBulletSpeed = "Upgrade bullet speed"
texts.upgradeDamage = "Upgrade damage"
texts.upgradeFireRate = "Upgrade fire rate"
texts.upgradeWeaponsRange = "Upgrade weapon's range"
--Weapons
texts.smg = "SMG"
texts.grenadeLauncher = "Grenade Launcher"
texts.katana = "Katana"
--Skills
texts.Speed = "Speed"

return texts
