local texts = {}

--MainMenu screen
texts.newGame = "Nowa Gra"
texts.settings = "Ustawienia"
texts.credits = "Twórcy"
texts.exit = "Wyjście"
texts.enableFullscreen = "Włącz pełny ekran"
texts.disableFullscreen = "Wyłącz pełny ekran"
texts.changeResolution = "Zmień rozdzielczość"
texts.back = "Cofnij"
texts.programmers = "Programiści"
texts.audioDesign = "Dźwięk"
--PauseMenu screen
texts.resume = "Kontynuuj"
--GameOver Screen
texts.dead = "Nie żyjesz!"
texts.pressToContinue = "Naciśnij dowolny klawisz, by kontynuować"
--Shop screen
texts.shop = "Sklep"
texts.continue = "Kontynuuj"
texts.buySmg = "Kup SMG"
texts.upgradeBulletSpeed = "Ulepsz prędkość pocisków"
texts.upgradeDamage = "Ulepsz obrażenia broni"
texts.upgradeFireRate = "Ulepsz szybkostrzelność broni"
texts.upgradeWeaponsRange = "Ulepsz zasięg broni"


return texts
