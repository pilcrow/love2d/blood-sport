local texts = {}

--MainMenu screen
texts.newGame = "新しいゲーム"
texts.settings = "設定"
texts.credits = "クレジット"
texts.exit = "出口"
texts.enableFullscreen = "フルスクリーンを有効にする"
texts.disableFullscreen = "フルスクリーンを無効にする"
texts.changeResolution = "解像度を変更する"
texts.back = "バック"
texts.programmers = "プログラマー"
texts.audioDesign = "オーディオデザイン"
--PauseMenu screen
texts.resume = "再開する"
--GameOver Screen
texts.dead = "あなたは死んでいる！"
texts.pressToContinue = "何かキーを押すと続行します"
--Shop screen
texts.shop = "ショップ"
texts.continue = "持続する"
texts.buySmg = "SMGを購入する"
texts.upgradeBulletSpeed = "弾丸の速度をアップグレードする"
texts.upgradeDamage = "アップグレードダメージ"
texts.upgradeFireRate = "アップグレード率"
texts.upgradeWeaponsRange = "武器の射程をアップグレードする"


return texts
