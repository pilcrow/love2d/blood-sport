return {
  version = "1.2",
  luaversion = "5.1",
  tiledversion = "1.2.4",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 128,
  height = 128,
  tilewidth = 32,
  tileheight = 32,
  nextlayerid = 11,
  nextobjectid = 12,
  properties = {},
  tilesets = {
    {
      name = "entities1",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 2,
      image = "../images/entities1.png",
      imagewidth = 64,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 4,
      tiles = {
        {
          id = 0,
          properties = {
            ["impassable"] = true
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "",
                shape = "rectangle",
                x = 3.375,
                y = 17.375,
                width = 23.75,
                height = 14.375,
                rotation = 0,
                visible = true,
                properties = {}
              },
              {
                id = 3,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16.125,
                y = 3.375,
                width = 12.125,
                height = 28,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 2,
          properties = {
            ["impassable"] = true
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "",
                shape = "rectangle",
                x = 3.625,
                y = 4.625,
                width = 24.25,
                height = 27.375,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 3,
          properties = {
            ["impassable"] = true
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "",
                shape = "rectangle",
                x = 4,
                y = 1.5,
                width = 21,
                height = 30.5,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        }
      }
    },
    {
      name = "castle_walls",
      firstgid = 5,
      filename = "../tilesets/castle_walls.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 8,
      image = "../images/castle_walls.png",
      imagewidth = 256,
      imageheight = 256,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 64,
      tiles = {
        {
          id = 2,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 8,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 10,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 16,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 17,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 24,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 25,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 32,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 33,
          properties = {
            ["impassable"] = true
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "",
                shape = "rectangle",
                x = 0.363636,
                y = 0.363636,
                width = 31.6364,
                height = 31.4545,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 34,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 37,
          properties = {
            ["impassable"] = true
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "",
                shape = "rectangle",
                x = 0.545455,
                y = 22.3636,
                width = 31.2727,
                height = 9.63636,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 40,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 41,
          properties = {
            ["impassable"] = true
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 3,
                name = "",
                type = "",
                shape = "rectangle",
                x = 0.545455,
                y = 0.545455,
                width = 31.2727,
                height = 31.4545,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 42,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 48,
          properties = {
            ["impassable"] = true
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "",
                shape = "rectangle",
                x = 22.5455,
                y = 0.545455,
                width = 9.45455,
                height = 31.4545,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 50,
          properties = {
            ["impassable"] = true
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 0.181818,
                y = 0.363636,
                width = 9.63636,
                height = 31.4545,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 56,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 57,
          properties = {
            ["impassable"] = true
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 0.363636,
                y = 0.363636,
                width = 31.6364,
                height = 9.27273,
                rotation = 0,
                visible = true,
                properties = {}
              }
            }
          }
        },
        {
          id = 58,
          properties = {
            ["impassable"] = true
          }
        }
      }
    },
    {
      name = "sand",
      firstgid = 69,
      filename = "../tilesets/sand.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 2,
      image = "../images/sand.png",
      imagewidth = 64,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 4,
      tiles = {}
    },
    {
      name = "Interior-Furniture",
      firstgid = 73,
      filename = "../tilesets/rooftop_interior_furniture.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 16,
      image = "../images/Interior-Furniture.png",
      imagewidth = 512,
      imageheight = 512,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 256,
      tiles = {
        {
          id = 68,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 69,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 70,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 84,
          properties = {
            ["impassable"] = "true"
          }
        },
        {
          id = 86,
          properties = {
            ["impassable"] = "true"
          }
        },
        {
          id = 100,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 101,
          properties = {
            ["impassable"] = true
          }
        },
        {
          id = 102,
          properties = {
            ["impassable"] = true
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      id = 8,
      name = "ground",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt2mFu4jAQBtDsNXofAvc/0WqVRnz9OsBPR9pX6QkEIZI19tgz6defbfsCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgv7Zv23bbnq/n+/u3W70+4vW89vFtj+/z2rz//fv96nHzjP8+xOxe8drjfV6Xcb3V5zlPco7cxf8y/sVk+jtj+m4dn/Hc6/ucQ+d15zw4f7t63Bz27WfuzljeKmZTrPvz3B86r1j/15Px7TWace/9PufIGdc+O7w6N9j/r6PXfsZ3336v7dwDch50fKe5k/Nr9bg59Fq9DTHPmOZ6z7Wec+KxzXMk77d63Bx6T+/5kPXdrWLaNcG7+dDniNXj5vCoGE81/vR5xjtze+8BWQtkTlk9bp7xzznQOTzzfu4Nfc6b+kWZF3J+WP/X0fV81239XeaJPg9M/aGeB85/19K1+tSr633gVa+w94DOBXnf1ePmMOXpjGfmh9zHs1bch/g+PsyB1ePm0M99Ohe86uH39b0/9Bkwr5f/r2Oq4bonuNf77g++2wP2ivtd/C/lVby7F9g5IGv63Cvy864RMm+sHjfP+He8O3dn3Kb+fvf4pj2/c8nqcXPoc/2jYp29nnfP/PtZYNcO/bvV4+YwPZ/p2E/9na7/uj/cvaPOMavHzaHXb/Z6+vn/p2fAuedPdWVes3rcHKbnfP08oHP3VOvn+p7OAX1WXD1uDn1Wn+r5qc7rvNC5oX/XOWP1uPkZ/97Tpxo/1/+rumE6L/Qck/+vI89muY9P/bvp+W/mi64H+nyY82L1uHnGv3t3r54D936f+aDvkT2DPj+I/3X0/+V0LTDFLtf/lAP6d1OvYPW4OXQsp95e14OPimvniVd7Q95z9bg5TM/yuqf7Ltaf/j+kf2P9X0uf8fssP/Xxpms7V/Sa77ywetwcum/Xe3ae7W7b7/Xdc6Kf9fT15/1XjxsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAID1/gLj4Vgi"
    },
    {
      type = "tilelayer",
      id = 9,
      name = "walls",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt0bsKwkAURdHpLK20006t1Eqs1P//K6cUMRhS3ATOWnCKhDwYdmsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADQ2rp4LMeh7/ix1df10L1N3/bPM0PfOJWcjDHOfZcJ2/XtJ757LTkZY9xm+Od9hn/ym/7Z9M+mfzb9s+mfTf9s+mfTP5v+2fTPpn82/bPpn03/bPpn0z+b/tn0z6Z/Nv2z6Z9N/2z6Z9M/m/7ZHn3P4r1KTgYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDSvQEwrxz7"
    },
    {
      type = "tilelayer",
      id = 10,
      name = "props",
      x = 0,
      y = 0,
      width = 128,
      height = 128,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzt1zkOwCAMBEAi8v83p0dxgVEUjpkKUSAa1kspAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAefffF2DYFezXD84EYA69GW/e06rBGoB5RfM8ynH/OuCNv8HadHc4x0iXk/Xn0f3PkX3fOgTAGrI5r//B3nq6nN4HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEDrAbKEAEE="
    },
    {
      type = "objectgroup",
      id = 6,
      name = "playerSpawn",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 2,
          name = "player",
          type = "",
          shape = "rectangle",
          x = 2039.5,
          y = 2008.5,
          width = 16.6667,
          height = 15,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      id = 7,
      name = "enemySpawn",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 4,
          name = "",
          type = "",
          shape = "rectangle",
          x = 2474.5,
          y = 2474.5,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 5,
          name = "",
          type = "",
          shape = "rectangle",
          x = 2477,
          y = 1681,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 6,
          name = "",
          type = "",
          shape = "rectangle",
          x = 2477,
          y = 2034,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 7,
          name = "",
          type = "",
          shape = "rectangle",
          x = 2047,
          y = 1676,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 8,
          name = "",
          type = "",
          shape = "rectangle",
          x = 2043,
          y = 2483,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 9,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1616,
          y = 2480,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 10,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1615,
          y = 2030,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 11,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1619,
          y = 1684,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
