<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="Roads" tilewidth="32" tileheight="32" tilecount="475" columns="25">
 <image source="../images/Street.png" width="800" height="608"/>
 <terraintypes>
  <terrain name="Dark Sidewalk" tile="433"/>
  <terrain name="Light Sidewalk" tile="436"/>
 </terraintypes>
 <tile id="11">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="86">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="87">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="224">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="226">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="249">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="274">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="299">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="303" terrain=",1,1,1"/>
 <tile id="311">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="312">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="320">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="321">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="324">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="328" terrain="1,,1,1"/>
 <tile id="345">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="346">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="349">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="353" terrain="1,1,1,"/>
 <tile id="363">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="368">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="369">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="370">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="371">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="374">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="378" terrain="1,1,,1"/>
 <tile id="386">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="387">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="393">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="394">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="395">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="396">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="399">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="404" terrain="0,0,0,"/>
 <tile id="405" terrain="0,0,,"/>
 <tile id="406" terrain="0,0,,0"/>
 <tile id="407" terrain=",,,0"/>
 <tile id="408" terrain=",,0,0"/>
 <tile id="409" terrain=",,0,"/>
 <tile id="410" terrain=",,,1"/>
 <tile id="411" terrain=",,1,1"/>
 <tile id="412" terrain=",,1,"/>
 <tile id="423">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="424">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="429" terrain="0,,0,"/>
 <tile id="431" terrain=",0,,0"/>
 <tile id="432" terrain=",0,,0"/>
 <tile id="433" terrain="0,0,0,0"/>
 <tile id="434" terrain="0,,0,"/>
 <tile id="435" terrain=",1,,1"/>
 <tile id="436" terrain="1,1,1,1"/>
 <tile id="437" terrain="1,,1,">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="454" terrain="0,,0,0"/>
 <tile id="455" terrain=",,0,0"/>
 <tile id="456" terrain=",0,0,0"/>
 <tile id="457" terrain=",0,,"/>
 <tile id="458" terrain="0,0,,"/>
 <tile id="459" terrain="0,,,"/>
 <tile id="460" terrain=",1,,"/>
 <tile id="461" terrain="1,1,,"/>
 <tile id="462" terrain="1,,,"/>
</tileset>
