<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="castle_walls" tilewidth="32" tileheight="32" tilecount="64" columns="8">
 <image source="../images/castle_walls.png" width="256" height="256"/>
 <tile id="2">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" x="0.363636" y="0.363636" width="31.6364" height="31.4545"/>
  </objectgroup>
 </tile>
 <tile id="34">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="37">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" x="0.545455" y="22.3636" width="31.2727" height="9.63636"/>
  </objectgroup>
 </tile>
 <tile id="40">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
  <objectgroup draworder="index">
   <object id="3" x="0.545455" y="0.545455" width="31.2727" height="31.4545"/>
  </objectgroup>
 </tile>
 <tile id="42">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" x="22.5455" y="0.545455" width="9.45455" height="31.4545"/>
  </objectgroup>
 </tile>
 <tile id="50">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0.181818" y="0.363636" width="9.63636" height="31.4545"/>
  </objectgroup>
 </tile>
 <tile id="56">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="57">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0.363636" y="0.363636" width="31.6364" height="9.27273"/>
  </objectgroup>
 </tile>
 <tile id="58">
  <properties>
   <property name="impassable" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
